<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reg_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function addData($data)
    {
        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }
    public function phone_check($data)
    {
        $this->db->select('phone');
        $this->db->from('users');
        $where=array('phone'=>$data);
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->result();
    }
    public function get_list($table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->result();
    }
}
?>