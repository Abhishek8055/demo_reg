<section id="register">

<div class="container">
  <div class="row">
    <div class="col-xs-10 col-sm-8 col-md-8 col-sm-offset-2 col-xs-offset-1 col-md-offset-2 box">
      <h2 class="text-center">Registered Users</h2>
      <div style="overflow: auto;">
      <table class="table table-bordered">
    <thead>
      <tr>
        <th>Sr. No</th>
        <th>Name</th>
        <th>Mobile</th>
        <th>Address</th>
        <th>City</th>
        <th>Pincode</th>
      </tr>
    </thead>
    <tbody>
      <?php $i=1; if($user_list){ foreach ($user_list as $value) { ?> 
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $value->name; ?></td>
          <td><?php echo $value->phone; ?></td>
          <td><?php echo $value->address; ?></td>
          <td><?php echo $value->city; ?></td>
          <td><?php echo $value->pincode; ?></td>
        </tr>
      <?php $i++; } }else{ ?>
        <tr>
          <td>No User Registered.</td>
        </tr>
      <?php  } ?>
    </tbody>
  </table>
        
      </div>
    </div>
  </div>
</div>
    </section>