<section id="register">

<div class="container">
  <div class="row">
    <div class="col-xs-10 col-sm-6 col-md-6 col-xs-offset-1 col-sm-offset-3 col-md-offset-3 box">
  <h2 class="text-center">Registration Form</h2>
  <form id="formReg" action="javascript:registration()">
    <div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" required="">
    </div>
  </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
      <label for="phone">Phone:</label>
      <input type="text" class="form-control" id="phone" placeholder="Enter Phone" name="phone"     pattern=".{10}" title="Phone No should 10 digits" required="">
    </div>
  </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <label for="address">Address:</label>
      <textarea id="address" class="form-control" name="address" placeholder="Enter Address"  required=""></textarea>
    </div>
  </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
      <label for="city">City:</label>
      <input type="text" class="form-control" id="city" placeholder="Enter City" name="city"  required="">
    </div>
  </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
      <label for="pincode">Pincode:</label>
      <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="pincode" pattern=".{6}" title="Pincode should 6 digits" required="">
    </div>
  </div>
    <div class="form-group">
  <center><button type="submit" class="btn btn-danger">Submit</button></center>  </div>  
  </form>      
    </div>
  </div>
</div>
    </section>
<script>
  
function registration() {
  var form = $('form#formReg')[0];
  var data = new FormData(form);
  $.ajax({
    type: "POST",
    url: "Welcome/regCheck",
    data: data,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function(response){
      var obj = JSON.parse(response);
      if(obj.success){
        alert(obj.message);
        window.location.reload();
      }else{      
        alert(obj.message);        
      }
    },
    error: function(jqXHR,textStatus,errorThrown){
        alert(obj.message);      
    }
  });
}
</script>