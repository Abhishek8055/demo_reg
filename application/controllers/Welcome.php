<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Reg_model');
		$this->db = $this->load->database('default', true);
	}

	public function header()
	{
		$this->load->view('header');
	}
	public function footer()
	{
		$this->load->view('footer');
	}
//start registration
	public function register()
	{
		$this->header();
		$this->load->view('register');
		$this->footer();
	}
	public function regCheck()
	{
			if($this->Reg_model->phone_check($this->input->post('phone'))){
				$success = false;
				$message = 'Phone No. already exist.';
			}else{				
				if($data = $this->Reg_model->addData($this->input->post())){	
					$success = true;
					$message = 'Welcome...You are Registered Successfully';
				}else{
					$success = false;
					$message = 'Failed to register!!!';
				}
			}
		echo json_encode(array('success' =>$success,'message' => $message));
	}
	public function table()
	{
		$this->header();
        $viewData['user_list'] = $this->Reg_model->get_list('users');
		$this->load->view('table',$viewData);
		$this->footer();
	}
//end registration	
}